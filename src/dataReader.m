close all;
clear;
clc;

% Importa i valori della misura selezionata dal file risultato
% Ogni misura ha un indice corrispondente al numero di riga nel file-1
% 4 - Average Precision
% 5 - Rprec
% 20 - Precision at 10
[m, g] = importMeasure(4);

%Effettua ANOVA e restituisce la ANOVA table
[~, tbl, sts] = anova1(m, g, 'off');
tbl

currentFigure = figure;
% Inverte ordine colonne per il boxplot
boxplot(m(:, end:-1:1), 'Labels', cellstr(g(end:-1:1)), ...
        'Orientation', 'horizontal', 'Notch','off', 'Symbol', 'ro')

% Imposta i Label
ax = gca;
%ax.XLabel.String = 'Precision at 10';
%ax.XLabel.String = 'Precision at Recall Base (RPrec)';
ax.XLabel.String = 'Average Precision (AP)';
ax.YLabel.String = 'Run';

% Effettua le comparazioni con il test Tukey HSD
figure
tukey = multcompare(sts, 'Alpha', 0.05, 'Ctype', 'hsd');
tukey

% Imposta i Label
currentFigure = gcf;
ax = gca;
%ax.XLabel.String = 'Precision at 10';
%ax.XLabel.String = 'Precision at Recall Base (RPrec)';
ax.XLabel.String = 'Average Precision (AP)';
ax.YLabel.String = 'Run';

function [measure, group] = importMeasure(measureIdx)
    %Ottiene la lista dei files nella sottocartella 'stats'
    fileList = ls('stats');
    %Numero di files
    numFiles = size(fileList);
    %Espressione per estrarre tre stringhe per ogni riga del file
    formatSpec = '%s%s%s%[^\n\r]';
    %Numero di misure per Topic
    numMeasures = 91;
    %Numero di Topic
    numTopics = 50;
    %Inizializzazione output
    measure = [];
    group = [];
    
    % Salta i primi due "file" (essendo sempre . e ..)
    for i=3:numFiles(1)
        
        %Apertura del file i-esimo
        filename = fullfile('C:', 'Users', 'Filippo', 'Desktop', ...
                            'IR', 'stats', fileList(i,:));
        fileID = fopen(filename);
        map = [];
        
        %Ignora la prima riga e crea un array orizzontale di celle, ogni
        %cella contiene una colonna del file (colonna1 = nome misure, 
        %colonna2 = topics, colonna3 = misure) 
        C = textscan(fileID, formatSpec, 'Delimiter', '	', ...
                     'MultipleDelimsAsOne', true, ...
                     'EndOfLine', '\r\n', 'headerlines', 1);

        %Inizializza una matrice di celle metteno come prima colonna il
        %nome delle misure
        T = C{1,1}(1:numMeasures);

        %Per ogni topic aggiungi una colonna a T con i valori delle misure
        for j = 0:numTopics-1
            T = [T C{1,3}(j*numMeasures+1:j*numMeasures+numMeasures)];
        end
        
        %T ora � una "tabella", le colonne sono i topic, le righe le varie misure
        
        %Per ogni topic seleziona solo la misura corrispondente a
        %measureIdx
        for k = 0:numTopics-1
            map = [map str2double(T{measureIdx,2+k})];
        end
        
        %Aggiungi le misure della run corrente all'output
        measure = [measure; map];
        %Aggiungi il nome della run (nome file senza estensione) all'insieme dei gruppi
        group = [group strtok(string(fileList(i,:)), '.')];
        %Chiudi il file
        fclose(fileID);
        
    end
    
    %Trasponi
    measure = measure';
    
    %Calcola Media
    measureMean = mean(measure);
    %Ordina la media in modo decrescente e ricava l'ordinamento
    [~, idx] = sort(measureMean, 'descend');
    %Ordina measure e group
    measure = measure(:, idx);
    group = group(idx);
    
end