**Repository per Homework 1**

Le cartelle hanno il seguente contenuto:

- plot: Boxplot e confronti di Tukey HSD in formato .eps, .png, .fig
- run: file risultati delle run con i file .property di Terrier utilizzati.
- src: codice dello script Matlab per l'analisi statistica e i plot.
- stats: output della valutazione con trec_eval.